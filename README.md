# Beancount

## Summary

A minor mode that can be used to edit beancount input files.
Original project can be found at: [bitbucket.org/blais/beancount](https://bitbucket.org/blais/beancount/src/default/editors/emacs/)

## Installing

You will need Emacs 24+, `make` and [Cask](https://github.com/cask/cask) to
build the project.

    cd beancount
    make && make install

## License

See [COPYING][].

[COPYING]: ./COPYING
